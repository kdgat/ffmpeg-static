ffprobe-static
====

Static binaries for `ffprobe`.

Based on <https://github.com/eugeneware/ffmpeg-static>.

Binaries are from <http://ffmpeg.zeranoe.com/builds/>

Usage
----

```js
var ffprobe = require('ffprobe-static');
console.log(ffprobe.path);
```

Version Notes
----

Currently supports Mac OS X (Intel 64-bit and ARM), Linux (32, 64-bit and ARM) and Windows
(32 and 64-bit).

Currently version `4.4.0` is installed for Mac, Windows and Linux.<br>
**Multiple architectures have been removed from this release to make publishing possible**

I pulled the versions from the ffmpeg static build pages linked from the
official ffmpeg site. Namely:

* [64 bit Mac OSX](https://evermeet.cx/ffmpeg/)
* [ARM64 Mac OSX](https://evermeet.cx/ffmpeg/)
* [64 bit Linux](http://johnvansickle.com/ffmpeg/)
* [32 bit Linux](http://johnvansickle.com/ffmpeg/)
* [64 bit Windows](https://www.gyan.dev/ffmpeg/builds/)

Acknowledgements
----

Special thanks to [eugeneware](https://github.com/eugeneware) for <https://github.com/eugeneware/ffmpeg-static>, which this is based upon.

Thanks to the original [`ffprobe-static` package](https://github.com/joshwnj/ffprobe-static) and its creator joshwnj.
